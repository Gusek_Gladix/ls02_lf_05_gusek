import java.util.ArrayList;


public class Ladung{

	private Integer Vorhanden = 0;
	private ArrayList<String> Ladung = new ArrayList<String>();
	// Konstruktor f�r Ladung, wenn nichts vorgegeben ist.
	public Ladung() {
		setLadung("Nichts",0);
	}
	// Konstruktor f�r Ladung, wenn etwas �bergeben wird.
	public Ladung(String Wert, Integer cAnzahl) {
		setLadung(Wert, cAnzahl);
	}
	
	// Methode zum hinzuf�gen einer neuen Ladung.
	// Sie �berpr�ft ob die Ladung bereits exsistiert und addiert die anzahl.
	// Sie �berpr�ft ob es eine Ladung "Nichts" gibt und ob die aktuelle Ladung was anderes als "Nichts" enth�lt. wenn ja, dann l�scht sie "Nichts".
	public void setLadung(String ladung, Integer anzahl) {
		Integer Index = 0;
		for (int i = 0 ; i < Ladung.size() ; i++) {
			String[] beladung_mit_anzahl = Ladung.get(i).split(";");
			if (beladung_mit_anzahl[0].equals(ladung)) {
				anzahl = anzahl + Integer.parseInt(beladung_mit_anzahl[1]);				 
				Ladung.remove(i);
			}
			if (beladung_mit_anzahl[0].equals("Nichts") && !ladung.equals("Nichts")) {
				Ladung.remove(i);
			}
		}
			
		if (Vorhanden == 0) {
			Ladung.add(ladung + ";" + anzahl);
		}else {
			Ladung.remove(Index);
			Ladung.add(ladung + ";" + anzahl);
			Vorhanden = 0;
		}

	}
	// Gibt die aktuelle Ladungs Liste zur�ck.
	public ArrayList<String> getLadung(){
		return this.Ladung;
	}

}
