import java.util.Random;

public class Raumschiff extends Ladung{

	private Integer android;
	private String name;
	private String kapitaen;
	private Integer Photonentorpedos_Anzahl;
	private Integer energie;
	private Integer schilde;
	private Integer lebenserhaltungssysteme = 100;
	private Integer huelle;
	private String Logbuch = "";
	private String Meldung;
	//Konstruker f�rs Raumschiff, wenn nichts �bergeben wird.
	public Raumschiff() {
		super();
	}
	//Konstrukter f�rs Raumschiff, wenn etwas �bergeben wird. Ohne Ladung.
	public Raumschiff(String cname, String ckapitaen, Integer cPhotonentorpedos_Anzahl, Integer cenergie,Integer cschilde, Integer chuelle, Integer candroid) {
		super();
		this.setName(cname);
		this.setKapitaen(ckapitaen);
		this.setPhotonentorpedos_Anzahl(cPhotonentorpedos_Anzahl);
		this.setEnergie(cenergie);
		this.setSchilde(cschilde);
		this.setHuelle(chuelle);
		this.setAndroid(candroid);
	}
	//Konstrukter f�rs Raumschiff, wenn etwas �bergeben wird. Mit Landung.
	public Raumschiff(String Wert, Integer cAnzahl, String cname, String ckapitaen, Integer cPhotonentorpedos_Anzahl, Integer cenergie,Integer cschilde, Integer chuelle, Integer candroid) {
		super(Wert, cAnzahl);
		this.setName(cname);
		this.setKapitaen(ckapitaen);
		this.setPhotonentorpedos_Anzahl(cPhotonentorpedos_Anzahl);
		this.setEnergie(cenergie);
		this.setSchilde(cschilde);
		this.setHuelle(chuelle);
		this.setAndroid(candroid);
	}
	
	// Setzt den namen des Raumschiffes.
	public void setName(String sname) {
		name = sname;
		
	}
	
	// Gibt den namen des Raumschiffes zur�ck.
	public String getName() {
		return name;
	}
	// Setzt den namen des Kapit�ns.
	public void setKapitaen(String ckapitaen) {
		kapitaen = ckapitaen;
		
	} 
	
	// Gibt den namen des Kapit�ns zur�ck.
	public String getKapitaen() {
		return kapitaen;
	}
	
	// Setzt die Anzahl der Photonentorpedos.
	public void setPhotonentorpedos_Anzahl(Integer cwaffen_Anzahl) {
		Photonentorpedos_Anzahl = cwaffen_Anzahl;
		
	}
	
	public Integer getPhotonentorpedos_Anzahl() {
		return Photonentorpedos_Anzahl;
	}
	
	// Setzt die momentane Energie des Raumschiffes.
	public void setEnergie(Integer cenergie) {
		energie = cenergie;
		
	}
	
	public Integer getEnergie() {
		return energie;
	}
	
	// Setzt den momentanen Schild des Raumschiffes.
	public void setSchilde(Integer cschilde) {
		schilde = cschilde;
		
	}
	
	public Integer getSchilde() {
		return schilde;
	}
	
	// Setzt die momentane huelle des Raumschiffes.
	public void setHuelle(Integer chuelle) {
		huelle = chuelle;
		
	}
	public Integer getHuelle() {
		return huelle;
	}
	
	// Setzt die aktuelle Lebenserhaltungssysteme des Raumschiffes.
	public void setLebenserhaltungssysteme(Integer lebenserhaltung) {
		lebenserhaltungssysteme = lebenserhaltung;
	}
	public Integer getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}
	
	// Setzt die Aktuelle Anzahl der Androide.
	public void setAndroid(Integer Anzahl) {
		this.android = Anzahl;
	}
	public Integer getAndroid() {
		return this.android;
	}
	
	// Methode um Torpedos abzuschie�en.
	// Pr�ft ob Torpedos vorhanden sind.
	// Ruft Methode f�r den Treffer auf.
	public void doAbschussTorpedo(Raumschiff Gegner_Schiff) {
		if (this.getPhotonentorpedos_Anzahl() < 1) {
			this.Meldung = this.getName() + ": Keine Photonentorpedos gefunden!";
			this.add_logbuch(Meldung);
			System.out.println(Meldung);
			this.doNachricht(this.getName() + ": -=*Click*=-");
		}else {

			this.doNachricht("Das Raumschiff " + Gegner_Schiff.getName() +  " wurde mit Photonentorpedos abgeschossen.");
			this.setPhotonentorpedos_Anzahl((this.Photonentorpedos_Anzahl - 1 ));
			this.doTreffer(Gegner_Schiff);
		}
		
		// Ermitteln ob das Raumschiff �berhaupt exsistiert.
	}
	
// Methode um Phaser abzuschie�en.
// Pr�ft ob genug Energie vorhanden ist.
// Ruft Methode f�r den Treffer auf.
	public void doAbschussPhaser(Raumschiff Gegner_Schiff) {
		if (this.getEnergie() < 50) {
			this.doNachricht(this.getName() + ": -=*Click*=-");
		}else {

			this.doNachricht(this.getName() + ": Phaserkanone abgeschossen");
			this.setEnergie((this.getEnergie() - 50));
			this.doTreffer(Gegner_Schiff);
		}
		
		// Ermitteln ob das Raumschiff �berhaupt exsistiert.
	}
	
	// Wenn schilde vorhanden sind, werden sie abgezogen.
	// Wenn energie vorhanden ist und kein schild da ist, wird sie abgezogen.
	// Wenn huelle vorhanden ist, und kein schild da ist, wird sie abgezogen.
	// Wenn keine huelle vorhanden ist, wird Lebenserhaltungssysteme abgezogen.
	
	public void doTreffer(Raumschiff Schiff) {
		Schiff.setSchilde((Schiff.getSchilde() - 50));
		if (Schiff.getSchilde() <= 0) {
			Schiff.setEnergie((Schiff.getEnergie() - 50));
			Schiff.setHuelle((Schiff.getHuelle() - 50));
		}
		if (Schiff.getHuelle() <= 0) {
			Schiff.setLebenserhaltungssysteme((Schiff.getLebenserhaltungssysteme() - 50));
			if (Schiff.getLebenserhaltungssysteme() <= 0) {
				Schiff.doNachricht("Alle Lebenserhaltungssysteme  sind vernichtet!");
			}
			
		}
		// Wahrscheinlich folgt hier sp�ter code zum berechner der Treffer qoute.
		this.Meldung = Schiff.getName() + " wurde getroffen!";
		this.add_logbuch(Meldung);
		System.out.println(Meldung);
	}
	
	// Gibt eine Globale nachricht aus.
	public void doNachricht(String nachricht) {
		this.Meldung = "Nachricht an Alle: " + nachricht;
		this.add_logbuch(Meldung);
		System.out.println(Meldung);
	}
	
	// F�gt eine Nachricht ins Logbuch hinzu.
	public void add_logbuch(String nachricht) {
		this.Logbuch = this.Logbuch + nachricht + "\n";
	}
	// Liest das Gesamt Logbuch.
	public void lese_Logbuch() {
		System.out.println(" Logbuch wird gelesen....");
		System.out.println(this.Logbuch);

	}
	// Gibt den Zustand des Aktuellen Raumschiffes aus.
	public void doZustand() {
		this.Meldung ="Das ist das Raumschiff " + this.getName() + " Der Kapit�n ist " + this.getKapitaen() + " und hat " + this.getPhotonentorpedos_Anzahl() + " Photonentorpedos am Board, ihre Energie ist momentan bei " + this.getEnergie() + "% und die schildepunkte sind bei " + this.getSchilde() + "% und die h�lle ist bei " + this.getHuelle() + "%.";
		this.add_logbuch(Meldung);
		System.out.println(Meldung);
	}
	
	//Bel�d das Raumschiff mit der Anzahl an Torpedos.
	public void doBeladenTorpedo(Integer Anzahl) {
		this.setPhotonentorpedos_Anzahl(this.getPhotonentorpedos_Anzahl() + Anzahl);
		this.Meldung =  this.getName() +": [" + this.getPhotonentorpedos_Anzahl()  + "]Photonentorpedo(s) eingesetzt";
		this.add_logbuch(Meldung);
		System.out.println(Meldung);
	}
	
	// Repariert das Raumschiff mit einer Anzahl an Androiden.
	public void doRepair(boolean Energie, boolean Schild, boolean Huelle, Integer Androids) {
		Integer Repair = 0;
		Integer Schiffstrukturen = 0;
		if (this.getAndroid() < Androids) {
			Androids = this.getAndroid();
		}
		Random r = new Random();
		double rzahl = r.nextDouble()*100;
		if (Energie) {
			Schiffstrukturen++;
		}
		if (Schild) {
			Schiffstrukturen++;
		}
		if (Huelle) {
			Schiffstrukturen++;
		}
		if (Schiffstrukturen == 0) {
			
		}else {
			Repair = (((int)rzahl * Androids) / Schiffstrukturen);
		}
		if (Energie) {
			this.setEnergie(this.getEnergie() + Repair);
		}
		if (Schild) {
			this.setSchilde(this.getSchilde() + Repair);
		}
		if (Huelle) {
			this.setHuelle(this.getHuelle() + Repair);
		}
		
	}
	
	// Gibt das Ladungsverzeichnis des Raumschiffes aus.
	public void doLadungsverzeichnis() {
		this.Meldung = this.getName() + ": Ladungsverzeichnis wird ausgegeben:";
		this.add_logbuch(Meldung);
		System.out.println(Meldung);
		for(String beladung : this.getLadung()) {
			String[] beladung_mit_anzahl = beladung.split(";");
			if (beladung_mit_anzahl[1].equals("0")){
				this.Meldung = (beladung_mit_anzahl[0]);
				this.add_logbuch(Meldung);
				System.out.println(Meldung);
			}else {			
				this.Meldung = beladung_mit_anzahl[1] + " " + beladung_mit_anzahl[0];
				this.add_logbuch(Meldung);
				System.out.println(Meldung);
			}

		}
	}
	
	// R�umt das Ladungsverzeichnis auf, Sollte ein Item auf 0 sein.
	public void doAufr�umenLadungsverzeichnis() {
		for (int i = 0 ; i < this.getLadung().size() ; i++) {
			String[] beladung_mit_anzahl = this.getLadung().get(i).split(";");
			if (beladung_mit_anzahl[1].equals("0")) {			 
				this.getLadung().remove(i);
				}
		this.Meldung = this.getName() + ": Ladungsverzeichnis wurde aufger�umt";
		this.add_logbuch(Meldung);
		System.out.println(Meldung);
	 }	
	}
	
	//�berpr�ft ob Tropedos im Lager sind und ruft dann die Methode doBeladenTorpedo auf.
	// Sonst wird ausgegeben, dass es keine Torpedos gibt.
	public void doBeladenPhotonentorpedos() {
		Integer Gefunden = 0;
		for (int i = 0; i < this.getLadung().size(); i++) {
			String[] beladung_mit_anzahl = this.getLadung().get(i).split(";");
			if (beladung_mit_anzahl[0].equals("Photonentorpedo")) {
				this.doBeladenTorpedo(Integer.parseInt(beladung_mit_anzahl[1]));
				Gefunden = 1;
				this.getLadung().remove(i);
			}
		}
		if (Gefunden == 0) {
			this.Meldung = this.getName() + ": Keine Photonentorpedos zum beladen gefunden!";
			this.add_logbuch(Meldung);
			System.out.println(Meldung);
		}else {
			this.Meldung = this.getName() + ": Photonentorpedos geladen!";
			this.add_logbuch(Meldung);
			System.out.println(Meldung);
		}
  }
}

