import java.util.ArrayList;

public class Krieg_Der_Raumschiffe {

	public static void main(String[] args) {
		
		
		// TODO Auto-generated method stub
		Raumschiff romulaner = new Raumschiff("IRW Khazara","Tomalak",2,100,100,100,2);
		Raumschiff klingonen  = new Raumschiff("IKS Hegh'ta","Kruge",1,100,100,100,2);
		Raumschiff vulkanier  = new Raumschiff("Ni'Var","Spock",0,80,80,50,5);
		
		klingonen.setLadung("Ferengi Schneckensaft",200);
		klingonen.setLadung("Bat'leth Klingonen Schwert", 200);
		vulkanier.setLadung("Forschungssonde", 35);
		vulkanier.setLadung("Photonentorpedo", 3);
		romulaner.setLadung("Borg-Schrott", 5);
		romulaner.setLadung("Rote Materie", 2);
		romulaner.setLadung("Plasma-Waffe", 50);
		
		klingonen.doAbschussTorpedo(romulaner);
		romulaner.doAbschussPhaser(klingonen);
		vulkanier.doNachricht("Gewalt ist nicht logisch");
		klingonen.doZustand();
		klingonen.doLadungsverzeichnis();
		vulkanier.doRepair(true, true, true, 5);
		vulkanier.doBeladenPhotonentorpedos();
		vulkanier.doAufräumenLadungsverzeichnis();
		klingonen.doAbschussTorpedo(romulaner);

		klingonen.doZustand();
		klingonen.doLadungsverzeichnis();
		romulaner.doZustand();
		romulaner.doLadungsverzeichnis();
		vulkanier.doZustand();
		vulkanier.doLadungsverzeichnis();
	}

}

